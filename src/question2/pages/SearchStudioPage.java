package question2.pages;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SearchStudioPage extends BasePage {

	private WebDriver driver;

	public SearchStudioPage(WebDriver driver) {
		this.driver = driver;
	}
	final String searchMeeting="meetingSearch";
	final String zipCode="10011";
	

	public void clickStudioButton() {
		driver.findElement(By.linkText("Find a Studio")).click();
		waitForAnElement(driver,searchMeeting,"id");
	}
	
	public void searchMeetingUsingZipCode() {
		driver.findElement(By.id(searchMeeting)).sendKeys(zipCode);
		Actions a=new Actions(driver);
		a.sendKeys(Keys.ENTER).build().perform();
		//driver.manage().timeouts().implicitlyWait(3, TimeUnit.SECONDS);
	}
}
