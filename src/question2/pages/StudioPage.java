package question2.pages;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class StudioPage extends BasePage {
	
	private WebDriver driver;

	public StudioPage(WebDriver driver) {
		this.driver = driver;
	}

	final String locationName="//div[@class='location__name']";
	final String currentDay="//div[@class='hours-list-item-wrapper hours-list--currentday']";
	final String firstTitle="(//div[@class='location__name'])[1]";
	final String firstDistance="(//div[@class='location__distance'])[1]";
	final String dayOfWeek="(//div[@class='schedule-detailed-day-meetings'])";
	
	public void printTitleAndDistance() {
	   waitForAnElement(driver,firstTitle,"xpath");
	   String title=driver.findElement(By.xpath(firstTitle)).getText();
	   System.out.println(title);
		
		String distance=driver.findElement(By.xpath(firstDistance)).getText();
		System.out.println(distance);
	}
	
   public void openTitleAndVerify() {
		//click and open and verify
		String title=driver.findElement(By.xpath(firstTitle)).getText();
		driver.findElement(By.xpath(firstTitle)).click();
				
		String titleSecond=driver.findElement(By.xpath(locationName)).getText();
        assertEquals(title,titleSecond);
	}
	
    public void printOperationalHoursForToday() {
	      String todaysDate=driver.findElement(By.xpath(currentDay)).getText();
		  System.out.println("Todays operating hours:"+todaysDate);
		}
		
	
	public void printMeetings(String day) {
		day=day.toLowerCase();
		
		switch(day) {
			case "sun":
		        System.out.println(driver.findElement(By.xpath(dayOfWeek+"[1]")).getText());	
			break;
			
			case "mon":
				System.out.println(driver.findElement(By.xpath(dayOfWeek+"[2]")).getText());	
			break;
			
			case "tue":
				System.out.println(driver.findElement(By.xpath(dayOfWeek+"[3]")).getText());	
				
			break;
			
			case "wed":
				System.out.println(driver.findElement(By.xpath(dayOfWeek+"[4]")).getText());				
			break;
			
			case "thu":
				System.out.println(driver.findElement(By.xpath(dayOfWeek+"[5]")).getText());		
			break;
			
			case "fri":
				System.out.println(driver.findElement(By.xpath(dayOfWeek+"[6]")).getText());			
			break;
			
			case "sat":
				System.out.println(driver.findElement(By.xpath(dayOfWeek+"[7]")).getText());	
			break;
		}
    }
}
