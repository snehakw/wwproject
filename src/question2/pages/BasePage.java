package question2.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;

public class BasePage {
	
	
	protected WebDriver driver;
	
	@BeforeTest
	public void setUp() {

		System.setProperty("webdriver.chrome.driver", "D:\\Automation\\lib\\chromedriver.exe");
		driver = new ChromeDriver();
		String baseUrl = "https://www.weightwatchers.com/us/";
		driver.get(baseUrl);

	}
	
	public void waitForAnElement(WebDriver driver,String element,String locator) {
	 WebDriverWait wait =new WebDriverWait(driver, 20);
	 
	 switch(locator){
	 case "xpath":
	  wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath(element)));
     break;
	 case "id":
		 wait.until(ExpectedConditions.presenceOfElementLocated(By.id(element)));
	  break;
	 }
   }
}
