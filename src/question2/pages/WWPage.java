package question2.pages;

import static org.testng.Assert.assertEquals;

import org.openqa.selenium.WebDriver;

public class WWPage extends BasePage{

	private WebDriver driver;
	
	public WWPage(WebDriver driver) {
		this.driver=driver;
}
	public void verifyPageTitle(String expectedTitle) {
		String actualTitle = driver.getTitle();
		assertEquals(expectedTitle,actualTitle);
	}
}
