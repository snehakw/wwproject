package question2.tests;
import org.testng.annotations.AfterTest;
import org.testng.annotations.Test;
import question2.pages.SearchStudioPage;
import question2.pages.StudioPage;
import question2.pages.WWPage;

public class VerifySeleniumTest extends BaseTest {


	@Test
	public void verifyWWSampleTest() {
		
		WWPage ww = new WWPage(driver);
		ww.verifyPageTitle("WW (Weight Watchers): Weight Loss & Wellness Help");

		SearchStudioPage searchSp = new SearchStudioPage(driver);
		searchSp.clickStudioButton();
		ww.verifyPageTitle("Find WW Studios & Meetings Near You | WW USA");
		searchSp.searchMeetingUsingZipCode();

		StudioPage sp = new StudioPage(driver);
		sp.printTitleAndDistance();
		sp.openTitleAndVerify();
		sp.printOperationalHoursForToday();
		sp.printMeetings("Sat");
	}
	
	@AfterTest
	public void closeBrowser() {
		driver.close();
	}
}
