package question2.tests;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.BeforeTest;

public class BaseTest {
	
	WebDriver driver;
	@BeforeTest
	public void setUp() {

		System.setProperty("webdriver.chrome.driver", "..\\WWInterviewProject\\src\\resources\\chromedriver.exe");
		driver = new ChromeDriver();
		String baseUrl = "https://www.weightwatchers.com/us/";
		driver.get(baseUrl);

	}
}
