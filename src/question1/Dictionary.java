package question1;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class Dictionary {

	public static void main(String args[]) throws IOException {
		String path="..\\WWInterviewProject\\src\\resources\\WordsAndMeanings.txt";
		Dictionary dict=new Dictionary();
		dict.doesFileExist(path);
	}

	public void doesFileExist(String path) throws IOException {

		try {
			File file = new File(path);
			FileReader reader = new FileReader(file);
			BufferedReader br = new BufferedReader(reader);
            readFile(br);	
			br.close();
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		} catch (Exception e) {
			System.out.print(e + "Sorry! Please try again.");
		}
	}

	public void readFile(BufferedReader br) throws IOException {
		String line;
		while ((line = br.readLine()) != null) {
			String[] arr = line.split("-");
			System.out.println(arr[0]); // print key/word

			String[] arrMeanings = arr[1].split(",");   //print meanings

			for (int i = 0; i < arrMeanings.length; i++)
				System.out.println(arrMeanings[i]);		
	   }
	}
}
